build_and_push: # make build_and_push IMAGE=php TAG=8.1.0-fpm-alpine PASSWORD=***
	docker login registry.gitlab.com -u Rilex -p $(PASSWORD)
	docker build --pull -f $(IMAGE):$(TAG)/Dockerfile . -t registry.gitlab.com/rilex/docker-dev/$(IMAGE):$(TAG);
	docker push registry.gitlab.com/rilex/docker-dev/$(IMAGE):$(TAG);
	docker logout